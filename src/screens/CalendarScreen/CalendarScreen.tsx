import React, {useState} from 'react';
import {SafeAreaView} from 'react-native';
import {DateData} from 'react-native-calendars';

import styles from './styles';
import MonthlyCalendar from '../../componetns/Calendar/Calendar';

const calendarTheme = {
  selectedDayBackgroundColor: '#14CBDA',
  selectedDayTextColor: 'white',
  todayTextColor: '#14CBDA',
};

const CalendarScreen = () => {
  const [selectedDate, setSelectedDate] = useState<string>('');

  const handleDateSelect = (date: DateData) => {
    setSelectedDate(date.dateString);
  };
  return (
    <SafeAreaView style={styles.container}>
      <MonthlyCalendar
        handleDayPress={handleDateSelect}
        selected={selectedDate}
        theme={calendarTheme}
      />
    </SafeAreaView>
  );
};

export default CalendarScreen;
