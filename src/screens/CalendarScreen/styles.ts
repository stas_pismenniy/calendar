import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  segmentedControl: {
    marginVertical: 10,
    width: '80%',
  },
});
