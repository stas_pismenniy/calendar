import React from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';

import {NativeStackNavigationProp} from '@react-navigation/native-stack';

import styles from './styles';

type RootStackParamList = {
  Home: undefined;
  Calendar: undefined;
};

type NavigationProps<T extends keyof RootStackParamList> = {
  navigation: NativeStackNavigationProp<RootStackParamList, T>;
};

type HomeScreenProps = NavigationProps<'Home'>;

const HomeScreen = ({navigation}: HomeScreenProps) => {
  const navigateToCalendar = () => {
    navigation.navigate('Calendar');
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrapper}>
        <TouchableOpacity onPress={navigateToCalendar}>
          <Text>Navigate to Calendar</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default HomeScreen;
