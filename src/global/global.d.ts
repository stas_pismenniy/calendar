/** @description date function Type */
import {DateData} from 'react-native-calendars';

type DateChangeHandler = (newDate: string) => void;

/** @description Change period function Type */
type PeriodChangeHandler = (newPeriod: string) => void;

/** @description CalendarProps */
interface CalendarProps {
  onDateChange?: DateChangeHandler;
  onPeriodChange?: PeriodChangeHandler;
  theme?: CalendarTheme;
}

/** @description State Interface */
interface CalendarState {
  selectedDate: Date;
  selectedPeriod: string;
}
/** @description Calendar Props */
interface CalendarProps {
  handleDayPress: (date: DateData) => void;
  selected: string;
}

interface CalendarTheme {
  selectedDayBackgroundColor: string;
  selectedDayTextColor: string;
  todayTextColor: string;
}

interface WeeklyPeriod {
  startDay: string; // Initial day of the week (eg 'Monday')
  endDay: string; // End day of the week (eg 'Sunday')
}

interface DailyPeriod {
  startTime: string;
  endTime: string;
}

interface OnceEvery4WeeksPeriod {
  dayOfWeek: string; // Day of the week (eg 'Monday')
  startTime: string;
  endTime: string;
}
