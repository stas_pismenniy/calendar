import {View} from 'react-native';
import {Calendar} from 'react-native-calendars';
import React from 'react';
import {CalendarProps} from '../../global/global';

import styles from './styles';

const MonthlyCalendar = ({handleDayPress, selected, theme}: CalendarProps) => (
  <View>
    <Calendar
      onDayPress={handleDayPress}
      style={styles.container}
      markedDates={{
        [selected]: {
          selected: true,
          disableTouchEvent: true,
        },
      }}
      theme={theme}
    />
  </View>
);

export default MonthlyCalendar;
