import React from 'react';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {HomeScreen, CalendarScreen} from '../screens';
import routes from './routes';

const Stack = createNativeStackNavigator();

const MainStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name={routes.Home} component={HomeScreen} />
      <Stack.Screen name={routes.Calendar} component={CalendarScreen} />
    </Stack.Navigator>
  );
};

export default MainStack;
